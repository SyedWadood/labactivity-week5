#include <iostream>

using namespace std;

int main() {

    int i, j, upto;

    cout << "Find prime numbers upto : ";
    cin >> upto;

    cout << endl << "All prime numbers upto " << upto << " are : " << endl;

    for(i = 2; i <= upto; i++) {

        for(j = 2; j <= (i / 2); j++) {

            if(i % j == 0) {
                j = i;
                break;
            }
        }

        if(j != i) {
            cout << i << " ";
        }
    }

    return 0;
}